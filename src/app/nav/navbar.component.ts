import { Component, OnInit } from '@angular/core';
import { AuthService } from '../user/auth.service';
import { ISession, EventService } from '../events';

@Component({
    selector: "nav-bar",
    templateUrl: "./navbar.component.html",
    styles: [`
    .nav.navbar-nav {font-size  :15px;}
    #searchForm {margin-right:100px;},
    @media (max-width:1200px) {#searchForm {display:none}}
    li > a.active{ color:#F97924; }
    `]
})
export class NavbarComponent implements OnInit {
    authSErvice
    foundSessions: ISession[];
    searchItem: string = "";
    constructor(
        private authSer: AuthService,
        private eventService: EventService
    ) { }
    ngOnInit() {
        this.authSErvice = this.authSer;
    }

    /**
     * to search the session list.
     * @param searchItem text iteam enterd by user.
     */
    onSearchSession = (searchItem) => {
        this.eventService.searchSession(searchItem).subscribe(sessions => {
            this.foundSessions = sessions;
            // console.log('foundedSessions--------', this.foundSessions);
        });
    };
}