import { Component, Input } from '@angular/core';

@Component({
    selector: 'collapsable-well',
    template: `
        <div (click)="toggleContant()" class="well pointable">
        <h4><ng-content select="[well-title]"></ng-content></h4>
            <ng-content select="[well-body]"></ng-content>
    `
})
export class CollapsableWellComponent {
    visible: boolean = false;
    /**
     * to toggle the content.
     */
    toggleContant = () => this.visible = !this.visible;
}