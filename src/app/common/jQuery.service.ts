import { InjectionToken } from "@angular/core";


/**
 * service use the concept of injectionToken
 */
export let JQ_TOKEN = new InjectionToken<Object>('jQuery');  