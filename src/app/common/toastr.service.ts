import {
    // Injectable,
    InjectionToken
} from '@angular/core';
// this is another way to use the dependancy injection for service
export let TOASTR_TOKEN = new InjectionToken<Toastr>('toastr');

export interface Toastr {
    success(msg: string, title?: string): void;
    info(msg: string, title?: string): void;
    warning(msg: string, title?: string): void;
    error(msg: string, title?: string): void;
}







// declare let toastr: any;
// /**
//  * ToastrService for toastr Library for Taoster message.
//  * Note:-   this Service is Use accross the application.
//  */
// @Injectable()
// export class ToastrService {
//     /**
//      * to toast the success message.
//      */
//     success = (message: string, title?: string) => {
//         toastr.success(message, title)
//     }
//     /**
//      * to toast the info message.
//      */
//     info = (message: string, title?: string) => {
//         toastr.info(message, title)
//     }
//     /**
//      * to toast the Warning message.
//      */
//     warning = (message: string, title?: string) => {
//         toastr.warning(message, title)
//     }
//     /**
//      * to toast the error message.
//      */
//     error = (message: string, title?: string) => {
//         toastr.error(message, title)
//     }
// }