import { OnInit, Directive, ElementRef, Inject, Input } from '@angular/core';
import { JQ_TOKEN } from './jQuery.service';

/**
 * this directive is used to popup the modal
 */
@Directive({
    selector: '[modal-trigger]'
})
export class ModalTriggerDirective implements OnInit {
    private el: HTMLElement;
    @Input('modal-trigger') modalId: string;
    /**
     * this constructor hase to be requre the injection of 'ElementRef'.
     * @param ref for the reference of HTMLELEMENT on which the directive with given selectore is used.
     * @param $ for injection of jQuery token which is used in service.
     * 
     * Note:- 1)    it is not compalsory to inject the 'ElementRef',
     *              but if you are try to implements the custome directive
     *              you should inject the 'ElementRef' and use it accordingly.
     */
    constructor(
        ref: ElementRef,
        @Inject(JQ_TOKEN) private $: any
    ) {
        this.el = ref.nativeElement;
    }
    ngOnInit() {
        /**
         * to popup the modal on click event.
         */
        this.el.addEventListener('click', e => this.$(`#${this.modalId}`).modal({}));
    }
}