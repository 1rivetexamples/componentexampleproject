import { Component, OnInit } from '@angular/core';
import { AuthService } from './user/auth.service';

@Component({
  selector: 'app-root',
  template: `
  <nav-bar></nav-bar>
  <router-outlet></router-outlet>
  `
})
export class AppComponent implements OnInit {
  constructor(
    private authService: AuthService
  ) { }

  ngOnInit() {
    /**
     * to check the authentication for use the current user details through the app.
     */
    this.authService.checkAuthenticationStatus();
  }
}
