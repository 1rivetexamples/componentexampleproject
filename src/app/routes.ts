/**
 * common Routers.
 */

import { Routes } from '@angular/router';
import {
    EventListComponent,
    EventDetailsComponent,
    CreateEventComponent,
    EventListResolver,
    CreateSessionComponent,
    EventResolver
} from './events';
import { Error404Component } from './errors/404.component';

export const route: Routes = [
    {
        path: "events/create", component: CreateEventComponent,
        canDeactivate: ['canDeactivateCreateEvent']
    },
    {
        /**
         * Note:-   here, before rendaring of the 'EventListComponent'
         *          angular will resolve the data with the help of 'EventListResolver'
         *          and assign to 'events'.
         */
        path: "events", component: EventListComponent,
        resolve: { events: EventListResolver }
    },
    { path: "events/:id", component: EventDetailsComponent, resolve: { event: EventResolver } },
    { path: "404", component: Error404Component },
    { path: "events/create/session", component: CreateSessionComponent },
    { path: "", redirectTo: "/events", pathMatch: "full" },
    { path: "user", loadChildren: './user/user.module#UserModule' }

];