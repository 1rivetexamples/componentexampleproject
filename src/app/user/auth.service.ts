import { IUser } from './login.model';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { tap, catchError } from 'rxjs/operators';
import { of } from 'rxjs';

@Injectable()
export class AuthService {
    currentUser: IUser;

    constructor(
        private http: HttpClient
    ) { }

    /**
     * to assign the user details to current user.
     */
    login = (userName: string, password: string) => {
        let options = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) };
        return this.http.post('/api/login', { username: userName, password: password }, options)
            .pipe(tap(data => this.currentUser = <IUser>data['user']))
            .pipe(catchError(err => of(false)));
        // this.currentUser = {
        //     id: 1,
        //     firstName: "AFZAL",
        //     lastName: "SHEKHADA",
        //     userName: userName
        // };
    }

    /**
     * to get the current user Details 
     * to make the user login and for using the current user details through the app.
     */
    checkAuthenticationStatus = () => {
        this.http.get('/api/currentIdentity')
            .pipe(tap(data => {
                if (data instanceof Object)
                    this.currentUser = <IUser>data;
            }))
            .subscribe();
    }

    /**
     * to check if user is authenticated or not.
     */
    isAuthenticated = () => !!this.currentUser;
    /**
     * to update the current user Details.
     */
    updateCurrentUser = (firstName: string, lastName: string) => {
        this.currentUser.firstName = firstName;
        this.currentUser.lastName = lastName;
        let options = { headers: new HttpHeaders({ 'Content-Type': '/application/json' }) }
        return this.http.put(`/api/users/${this.currentUser.id}`, this.currentUser, options);
    }
    /**
     * to make the user logout using http request.
     */
    logout = () => {
        this.currentUser = undefined;
        let options = { headers: new HttpHeaders({ 'Content-Type': '/application/json' }) }
        return this.http.post('/api/logout', {}, options)
    }
}
