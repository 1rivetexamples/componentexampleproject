import { Component, OnInit, Inject } from '@angular/core'
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService } from './auth.service';
import { Router } from '@angular/router';
import { Toastr, TOASTR_TOKEN } from '../common/toastr.service';

@Component({
  templateUrl: './profile.component.html',
  styles: [`
  em{ float:right; color:#E05C65; padding-left:10px; },
  .error input { backgroud-color: #E3C3C5;  }
  .error :: -webkit-input-placeholder { color: #999;  }
  .error :: -moz-placeholder { color: #999;  }
  .error : -moz-placeholder { color: #999;  }
  .error ::ms-input-placeholder { color: #999;  }
  
  `]
})
export class ProfileComponent implements OnInit {
  profileForm: FormGroup;
  firstName: FormControl;
  lastName: FormControl;

  constructor(
    private authSErvice: AuthService,
    private route: Router,
    @Inject(TOASTR_TOKEN) private toastr: Toastr //here we are injecting the toastr object via @Inject() decorator.
  ) { }
  ngOnInit() {
    /**
     * this will initialize the Form by reactive approach.
     */
    this.firstName = new FormControl(this.authSErvice.currentUser.firstName, [Validators.required, Validators.pattern('[a-zA-Z].*')]);
    this.lastName = new FormControl(this.authSErvice.currentUser.lastName, Validators.required);
    this.profileForm = new FormGroup({
      firstName: this.firstName,
      lastName: this.lastName
    });
  }

  /**
   * to edit the current user detials
   */
  onEditProfile = (formValue) => {
    this.authSErvice.updateCurrentUser(formValue.firstName, formValue.lastName)
      .subscribe(() => {
        // this.route.navigate(['/events']);
        this.toastr.success('profile is updated successfully..');
      });
  }

  /**
   * to cancel the profile edit activity.
   */
  onCancel = () => {
    // console.log(this.profileForm);
    this.route.navigate(['/events']);
  }
  /**
   * to validate the firstname
   */
  validateFirstName = () => this.firstName.invalid && this.firstName.touched;
  /**
   * to validate the lastname.
   */
  validateLastName = () => this.lastName.invalid && this.lastName.touched;
  /**
   * to make the user logout and navigate the the login page.
   */
  onLogout = () => {
    this.authSErvice.logout()
      .subscribe(() => {
        this.route.navigate(['/user/login']);
      });
  }

}