import { Component } from '@angular/core';
import { AuthService } from './auth.service';
import { Router } from '@angular/router';

@Component({
    selector: 'user-login',
    templateUrl: "./login.component.html",
    styles: [`
    em{ float:right; color:#E05C65; padding-left:10px; }
    `]
})
export class LoginComponent {
    loginInvalid: boolean = false;
    mouseOverLogin: boolean;
    constructor(
        private authSErver: AuthService,
        private router: Router
    ) { }
    /**
     * to submit the user data to authenticate the user.
     * and navigate to appropriate place.
     */
    onSubmit = (FormsValues) => {
        this.authSErver.login(FormsValues.username, FormsValues.password)
            .subscribe(resp => {
                if (!resp)
                    this.loginInvalid = true;
                else
                    this.router.navigate(['/events']);
            });
    }
    /**
     * to cancel the Authentication by user.
     */
    onCancel = () => this.router.navigate(['/events']);
}