import { Injectable } from '@angular/core';
import { EventService } from './shared/events.service';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { map } from 'rxjs/operators'
import { Observable } from 'rxjs';
import { IEvent } from './shared';

@Injectable()
export class EventResolver implements Resolve<any>{

    constructor(
        private EventService: EventService
    ) { }
    /**
     * to get the event details of given event Id.
     * Note:- you can perform any operation before redering of the component.
     */
    resolve = (route: ActivatedRouteSnapshot) => this.EventService.getEvent(route.params['id']);
};