import { FormControl } from '@angular/forms';

/**
 * this function will helpfull for validating with restriction of given words.
 * @param words list of restricted words.
 */
export function restrictedWords(words) {
    return (control: FormControl): { [key: string]: any } => {
        /**
         * checking for enterd words are matching with given restricted words.
         * if it is matching then it will returns the object with restricted words
         * otherwise this will returns a null.
         */
        if (!words) return null;
        let invalidWords = words.map(w => control.value.includes(w) ? w : null)
            .filter(w => w != null);
        return (invalidWords && invalidWords.length > 0) ? { 'restrictedWords': invalidWords.join(', ') } : null;
    }
}