import { Component, OnInit } from '@angular/core';
import { EventService } from './shared/events.service';
// import { ToastrService } from '../common/toastr.service';
import { ActivatedRoute } from '@angular/router';
import { IEvent } from './shared';

@Component({
    selector: "event-list",
    template: `
    <img src="/assets/images/ng-icon.png" />
<br>
<h4>Event Details</h4>
<hr>
<div class="row">
    <div class="col-md-5" *ngFor="let event of events" >
    <!-- (click) = "onThumbnailClick(event.name);" -->
    <event-thumbnails #thumnailsVar  [events] = "event" 
    (onEventClick)="onReciveEvent($event);" ></event-thumbnails>
    <!-- <button (click)="onLocalVarableChecking(thumnailsVar);" 
        class="btn btn-primary" >Checking local variable</button> -->
    </div>
</div>
<br>
    `
})
/**
 * implementing the lifecycle hooks.
 */
export class EventListComponent implements OnInit {
    events: IEvent[];
    /**
     * 
     * @param eventService injecting an Event Service.
     * @param toastr injecting toasterService.
     */
    constructor(
        private eventService: EventService,
        // private toastr: ToastrService,
        private route: ActivatedRoute
    ) { }
    /**
     * Note :- LifeCycle hook method doesn`nt support the ES6 syntax. 
     */
    ngOnInit() {
        this.events = this.route.snapshot.data['events'];
    }

    /**
     * this method is use to handle the output method 
     * which is passed by the chiled component.
     */
    onReciveEvent = (data) => console.log("dat-----", data.name);

    /**
     * this method is use to check the local reference varable from HTML template.
     * Note:-   the 'onLocalVariableCheck' method is of 'EventThumbnailsComponent'.
     *          this means you can access the public property and methods of component using 
     *          local reference varible of HTML template by component selector.
     */
    onLocalVarableChecking = (localVariable) => localVariable.onLocalVariableCheck();
    /**
     * to taost the success message while clicking on event Thumbnail grid.
     */
    // onThumbnailClick = (EventName) => this.toastr.success(EventName);
}