import { Validator, FormGroup, NG_VALIDATORS } from '@angular/forms';
import { Directive } from '@angular/core';

/**
 * to validate the location (address) details,
 * Note:- 1) if location details is not provided then user must enter the onlineURL.
 * location details contains the address,city and country fields.
 */
@Directive({
    selector: '[validateLocation]',
    providers: [{ provide: NG_VALIDATORS, useExisting: LocationValidator, multi: true }]
})
export class LocationValidator implements Validator {
    validate(formGroup: FormGroup): { [key: string]: any } {
        let addressControl = formGroup.controls['address'];
        let cityControl = formGroup.controls['city'];
        let counntryControl = formGroup.controls['country'];
        let onlineUrlControl = (<FormGroup>formGroup.root).controls['onlineUrl'];

        if ((addressControl && addressControl.value && cityControl && cityControl.value && counntryControl && cityControl.value) ||
            (onlineUrlControl && onlineUrlControl.value))
            return null;
        else
            return { validateLocation: true };
    }
}