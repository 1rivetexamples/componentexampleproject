import { SessionListComponent } from "./session-list.component";
import { ISession } from '../shared';

// testing the Component with isolated testing.

describe('SessionListComponent', () => {
    let component: SessionListComponent;
    let mockAuthService, mockVoterService;

    beforeEach(() => {
        component = new SessionListComponent(mockAuthService, mockVoterService);
    });

    describe('ngOnChanges', () => {
        it('should filter the session correctly', () => {
            component.sessions = <ISession[]>[
                { name: 'session 1', level: 'intermediate' },
                { name: 'session 3', level: 'intermediate' },
                { name: 'session 2', level: 'beginner' },
            ];

            component.filteredBy = 'beginner';
            component.sortBy = 'name';
            component.evnetID = 3;

            component.ngOnChanges();

            expect(component.visibleSession.length).toBe(1);
        });
        it('should sort the session correctly', () => {
            component.sessions = <ISession[]>[
                { name: 'session 1', level: 'intermediate' },
                { name: 'session 3', level: 'intermediate' },
                { name: 'session 2', level: 'beginner' },
            ];

            component.filteredBy = 'all';
            component.sortBy = 'name';
            component.evnetID = 3;

            component.ngOnChanges();

            expect(component.visibleSession[1].name).toBe("session 2");
        });
    })
});