import { ComponentFixture, TestBed, async } from "@angular/core/testing";
import { By } from '@angular/platform-browser';
/**
 * Note:-   1)  You need to be very carefull while testing with Shallow Integreted Testing.
 *              Becouse 'NO_ERROR_SCHMEA' will also hide the other depedencies which may
 *              lead to error for the code execution of actual requrement of code.
 *              for example :- if you have used the 'NgModel' in component
 *                              and you forgote to import the 'FormsModule' in Module
 *                              so, this will not provide the static error but 
 *                              this may affect on run time execution.
 */
import { DebugElement, NO_ERRORS_SCHEMA } from '@angular/core';

import { SessionListComponent } from './session-list.component';
import { AuthService } from 'src/app/user/auth.service';
import { VoterService } from './voter.service';
// import { UpvoteComponent } from './upvote.component'; // if this is commented that means this is tested with Shallow Integreted Test by 'NO_ERROR_SCHMEA' from @angular/core
import { DurationPipe } from '../shared';
// import { CollapsableWellComponent } from 'src/app/common';  // if this is commented that means this is tested with Shallow Integreted Test by 'NO_ERROR_SCHMEA' from @angular/core

describe('SessionListComponent', () => {
    let fixture: ComponentFixture<SessionListComponent>,
        component: SessionListComponent,
        element: HTMLElement,
        debugEl: DebugElement;

    /**
     * to initailise the dependencies and declarations before testing the which required for testing.
     */
    beforeEach(async(() => {
        let mockAuthService = {
            isAuthenticated: () => false,
            currentUser: { userName: 'joe' }
        };
        let mockVoterService = {
            userHaseVoted: () => true
        };

        TestBed.configureTestingModule({
            imports: [],
            declarations: [
                SessionListComponent,
                // UpvoteComponent,  // if this is commented that means this is tested with Shallow Integreted Test by 'NO_ERROR_SCHMEA' from @angular/core
                DurationPipe,
                // CollapsableWellComponent  // if this is commented that means this is tested with Shallow Integreted Test by 'NO_ERROR_SCHMEA' from @angular/core
            ],
            providers: [
                { provide: AuthService, useValue: mockAuthService },
                { provide: VoterService, useValue: mockVoterService }
            ],
            schemas: [
                NO_ERRORS_SCHEMA
            ]
        });
    }));// end of beforeEach with async

    beforeEach(() => {
        fixture = TestBed.createComponent(SessionListComponent);
        component = fixture.componentInstance;
        debugEl = fixture.debugElement;
        element = fixture.nativeElement;
    });// beforeEach

    /**
     * to give the values for testing and check the results accordingly. 
     */
    describe('initial display', () => {
        it('should have the correct session title', () => {
            /**
             * to give the values for testing.
             */
            component.sessions = [{
                id: 3,
                name: 'Session 1',
                presenter: 'joe',
                duration: 1,
                level: 'beginner',
                abstract: 'abstract',
                voters: ['john', 'bob']
            }];
            component.filteredBy = "all";
            component.sortBy = "name";
            component.evnetID = 4;

            component.ngOnChanges();
            fixture.detectChanges();
            /**
             * to test the requirement according it`s expectations.
             */
            // expect(element.querySelector('[well-title]').textContent).toContain('Session 1');
            expect(debugEl.query(By.css('[well-title]')).nativeElement.textContent).toContain('Session 1');
        })
    });// end of 'initial display'
});// end of 'SessionListComponent'