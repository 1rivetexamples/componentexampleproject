import { Component, Input, OnChanges } from '@angular/core';
import { ISession } from '../shared';
import { AuthService } from 'src/app/user/auth.service';
import { VoterService } from './voter.service';

@Component({
    selector: 'session-list',
    templateUrl: "./session-list.component.html"
})
export class SessionListComponent implements OnChanges {
    @Input() sessions: ISession[];
    @Input() filteredBy: string;
    @Input() sortBy: string;
    @Input() evnetID: number;
    visibleSession: ISession[] = [];

    constructor(
        private authSErvice: AuthService,
        private voterService: VoterService
    ) { }
    ngOnChanges() {
        if (this.sessions) {
            /**
             * this will filter the session list by 'name' or 'votes'
             */
            this.filteredSession(this.filteredBy);
            this.sortBy === 'name' ? this.visibleSession.sort(sortByNameAsc) : this.visibleSession.sort(sortByVotesDesc);
        }
    }

    /**
     * to make vote button executable 
     * which can be use to give a vote.
     * @param session to add or delete the username in voters array for making a confirmation of votes.
     */
    toggleVote = (session: ISession) => {
        if (this.userHasVoted(session)) {
            this.voterService.deleteVoter(this.evnetID, session, this.authSErvice.currentUser.userName);
        } else {
            this.voterService.addVoter(this.evnetID, session, this.authSErvice.currentUser.userName);
        }
        if (this.sortBy === 'votes') {
            this.visibleSession.sort(sortByVotesDesc);
        }
    }

    /**
     * to check that user hase voted for or not.
     * @param session for checking the user hase voted by voters array of session
     */
    userHasVoted = (session: ISession) => {
        return this.voterService.userHaseVoted(session, this.authSErvice.currentUser.userName);
    }

    /**
     * to filter the session list by given Option.
     * Note:- the filter Options should be following options.
     *          1) all
     *          2) beginner
     *          3) intermediate
     *          4) advanced
     * @param filter filter option.
     */
    filteredSession = (filter) => {
        if (filter === 'all') this.visibleSession = this.sessions.slice(0);
        else this.visibleSession = this.sessions.filter(session => session.level.toLocaleLowerCase() === filter);
    }
}

/**
 * to sort the session list by session name.
 * @param s1 first session object to comparing with second sessoin object.
 * @param s2 second session object to compare.
 */
function sortByNameAsc(s1: ISession, s2: ISession) {
    if (s1.name > s2.name) return 1;
    else if (s1.name === s2.name) return 0;
    else return -1;
}

/**
 * to sort the session list by it`s voters.
 * @param s1 first session object to comparing with second sessoin object.
 * @param s2 second session object to compare.
 */
function sortByVotesDesc(s1: ISession, s2: ISession) {
    return s2.voters.length - s1.voters.length;
}