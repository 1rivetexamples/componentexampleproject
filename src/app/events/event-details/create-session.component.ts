import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ISession, restrictedWords } from '../shared';

@Component({
    selector: "create-session",
    templateUrl: "./create-session.component.html",
    styles: [`
    em{ float:right; color:#E05C65; padding-left:10px; },
    .error input { backgroud-color: #E3C3C5;  }
    .error :: -webkit-input-placeholder { color: #999;  }
    .error :: -moz-placeholder { color: #999;  }
    .error : -moz-placeholder { color: #999;  }
    .error ::ms-input-placeholder { color: #999;  }
    
    `]
})
export class CreateSessionComponent implements OnInit {
    @Output() onCancelMode = new EventEmitter;
    @Output() onAddSession = new EventEmitter;
    name: FormControl;
    presenter: FormControl;
    duration: FormControl;
    level: FormControl;
    abstract: FormControl;
    SessionForm: FormGroup
    constructor(
        private router: Router
    ) { }

    ngOnInit() {
        this.name = new FormControl('', Validators.required);
        this.presenter = new FormControl('', Validators.required);
        this.duration = new FormControl('', [Validators.required]);
        this.level = new FormControl('', Validators.required);
        this.abstract = new FormControl('', [Validators.required, Validators.maxLength(20), restrictedWords(['foo', 'bars'])]);
        this.SessionForm = new FormGroup({
            name: this.name,
            presenter: this.presenter,
            duration: this.duration,
            level: this.level,
            abstract: this.abstract
        });
    }
    onCreateSession = (formValue) => {
        let session: ISession = {
            id: undefined,
            name: formValue.name,
            presenter: formValue.presenter,
            duration: + formValue.duration,
            level: formValue.level,
            abstract: formValue.abstract,
            voters: []
        };
        this.onAddSession.emit(session);
    }
    onCancel = () => {
        this.onCancelMode.emit();
    }

}