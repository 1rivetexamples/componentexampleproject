import { Component, OnInit } from '@angular/core';
import { EventService } from '../shared/events.service';
import { ActivatedRoute, Params } from '@angular/router';
import { IEvent, ISession } from '../shared';

@Component({
    templateUrl: "./event-details.component.html",
    styles: [`
    .container { pedding-left:20px; pedding-right:20px; }
    .event-image { height:100px;}
    a{ cursor:pointer; }
    `]
})
export class EventDetailsComponent implements OnInit {
    event: IEvent;
    addMode: boolean = false;
    filteredBy: string = 'all';
    sortBy: string = 'votes';
    constructor(
        private EventService: EventService,
        private activateRoute: ActivatedRoute
    ) { }
    ngOnInit() {
        /**
         * this will be executed while you navigating this
         * page for the first time.
         */
        // this.event = this.EventService.getEvent(+this.activateRoute.snapshot.params['id']);

        /**
         * this will be helpfull while you navigating
         * to this page from the same page,
         * so this will be update the params data while 
         * navigation command is executed.
         */
        this.activateRoute.data.forEach((data) => {
            this.event = data['event'];
            this.addMode = false;
        });// end of forEach
    }
    /**
     * enabling to add the session on event
     */
    addSession = () => {
        this.addMode = true;
    }
    /**
     * handle the cancel button to cancel the adding a session on event.
     */
    onCancelAddMode = (event) => this.addMode = false;
    /**
     * to Add the session on Event.
     */
    onAddSession = (session: ISession) => {
        const nexId = Math.max.apply(null, this.event.sessions.map(s => s.id));
        session.id = nexId + 1;
        this.event.sessions.push(session);
        this.EventService.addEvent(this.event).subscribe();
        // this.EventService.updateEvent(this.event);
        this.addMode = false;
    }
}