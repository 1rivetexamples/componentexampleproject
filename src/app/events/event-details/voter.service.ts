import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { Observable, of } from 'rxjs';

import { ISession } from '../shared';
import { Injectable } from '@angular/core';

@Injectable()
export class VoterService {
    constructor(
        private http: HttpClient
    ) { }

    /**
     * to filter the voters array for given username is not to be there.
     * @param eventId event Id will specifies the perticular event
     * @param session session object to delete the name of voter from it`s voters array.
     * @param voterName the name of voter (in this case 'username') to delete the of filter the voters array
     */
    deleteVoter = (eventId: number, session: ISession, voterName: string) => {
        session.voters = session.voters.filter(voter => voter !== voterName)
        const url = `/api/events/${eventId}/sessions/${session.id}/voters/${voterName}`;
        this.http.delete(url)
            .pipe(catchError(this.handleError('deleteVoter')))
            .subscribe();
    };

    /**
     * to add the vaterName in vater array of given session object of given eventId
     * @param eventId id of event to get the session object
     * @param session session object to add the voterName in voters Array.
     * @param voterName name of voter (in this case username) to add it in voters Array of session object.
     */
    addVoter = (eventId: number, session: ISession, voterName: string) => {
        session.voters.push(voterName);
        const options = { headers: new HttpHeaders({ 'Content-Type': '/application/json' }) };
        const url = `/api/events/${eventId}/sessions/${session.id}/voters/${voterName}`;
        this.http.post(url, {}, options)
            .pipe(catchError(this.handleError('addVoter')))
            .subscribe();
    };
    /**
     * to check the given voterName is availbale in voters array of given session.
     * if it is availbale in array then this will returns the true or else will returns the false.
     * @param session session object to checks the voters array.
     * @param voterName username to check that the usename is available in vaters array or not.
     */
    userHaseVoted = (session: ISession, voterName: string) => session.voters.some(voter => voter === voterName);

    /**
     * to handle the error while working with the Http
     * @param operation 
     * @param results 
     */
    handleError<T>(operation = 'operation', results?: T) {
        return (error: any): Observable<T> => {
            console.log(error);
            return of(results as T);
        }
    }
}