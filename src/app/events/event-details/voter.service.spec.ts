import { VoterService } from "./voter.service";
import { of } from 'rxjs';
import { ISession } from '../shared';
// testing service with Mock Call.
/**
 * to describe the voterService for testing.
 * this 'voterService' name will be display on the browser testing section.
 */
describe('voterService', () => {
    let voterService: VoterService,
        mockHttp;

    beforeEach(() => {
        // dummy Http Object.
        mockHttp = jasmine.createSpyObj('mockHttp', ['delete', 'post']);
        voterService = new VoterService(mockHttp);
    })

    /**
     * to test the deleteVoter method of VoterService.
     */
    describe('deleteVoter', () => {
        /**
         * to test the local voters array of session object.
         */
        it('should remove the voter from the list of voters', () => {

            // dummy object
            var session = { id: 6, voters: ['joe', 'john'] };

            /**
             * this will call the delete method of http 
             */
            mockHttp.delete.and.returnValue(of(false));

            /**
             * to execute the deleteVoter method of voterService
             */
            voterService.deleteVoter(3, <ISession>session, "joe");

            /**
             * to expect the output according to the dummy object.
             * Note:-1) this will decide whether the test to be pass or fail.
             */
            expect(session.voters.length).toBe(1);
            expect(session.voters[0]).toBe('john');
        });
        /**
         * to test the URL of execution of delete request of HTTP for deleteVoter method.
         */
        it('should call http.delete with the right URL', () => {
            var session = { id: 6, voters: ['joe', 'john'] };
            mockHttp.delete.and.returnValue(of(false));
            voterService.deleteVoter(3, <ISession>session, "joe");

            /**
             * to excecute the http.delete mathod of voterService of deleteVoters
             * and this will executed with URL.
             */
            let URL = '/api/events/3/sessions/6/voters/joe';// URL to check the url for excection of URL and comparing the expected URL.
            expect(mockHttp.delete).toHaveBeenCalledWith(URL);
        });
    });

    /**
     * to test the addVoter method of voterService.
     */
    describe('addVoter', () => {
        /**
         * to test the URL execution of post request of HTTP for addVoter method.
         */
        it('should call http.post with the right URL', () => {
            var session = { id: 6, voters: ['john'] };
            mockHttp.post.and.returnValue(of(false));

            voterService.addVoter(3, <ISession>session, 'joe');
            let URL = '/api/events/3/sessions/6/voters/joe';
            /**
             * this will check the execution of URL of post request of addVoter method.
             */
            expect(mockHttp.post).toHaveBeenCalledWith(URL, {}, jasmine.any(Object));
        });
    });
});
