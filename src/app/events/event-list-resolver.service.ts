import { Injectable } from '@angular/core';
import { EventService } from './shared/events.service';
import { Resolve } from '@angular/router';
import { map } from 'rxjs/operators'
import { Observable } from 'rxjs';
import { IEvent } from './shared';

@Injectable()
export class EventListResolver implements Resolve<any>{

    constructor(
        private EventService: EventService
    ) { }
    /**
     * to get the events data and map it to this resolver.
     * Note:- you can perform any operation before redering of the component.
     */
    resolve = () => this.EventService.getEvents();
};