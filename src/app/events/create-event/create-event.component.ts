import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { IEvent, EventService } from '../shared';

@Component({
    templateUrl: "./create-event.component.html",
    styles: [`
    em{ float:right; color:#E05C65; padding-left:10px; },
  .error input { backgroud-color: #E3C3C5;  }
  .error :: -webkit-input-placeholder { color: #999;  }
  .error :: -moz-placeholder { color: #999;  }
  .error : -moz-placeholder { color: #999;  }
  .error ::ms-input-placeholder { color: #999;  }
  
    `]
})
export class CreateEventComponent {
    isDirty: boolean = true;
    newEventForm: IEvent
    constructor(
        private router: Router,
        private eventServie: EventService
    ) { }
    /**
     * cancel button method.
     */
    onCancel = () => {
        this.router.navigate(['/events']);
    }
    /**
     * to save the event Details (i.e. will add the new Event Object to EVENT array.)
     * and this will navigate to events page.
     * @param formValues the new event details objct.
     */
    saveEvent = (formValues) => {
        // console.log('form', formValues);
        this.router.navigate(['/events']);
        this.eventServie.addEvent(formValues).subscribe();
        this.isDirty = false;
    }
}