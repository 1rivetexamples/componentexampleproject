import { EventEmitter, Component, Input, Output } from '@angular/core';
import { IEvent } from './shared';

@Component({
    selector: "event-thumbnails",
    template: `
    <div [routerLink]="['/events',events.id]" class="well hoverwell thumbnail" >
    <table >
    <tr>
        <td>Name :-</td>
        <td>{{events?.name | uppercase }}</td>
    </tr>
    <tr>
        <td>Date :-</td>
        <td>{{events?.date | date:'shortDate' }}</td>
    </tr>
    
    <tr>
    <div [ngSwitch]="events?.time" >
            <td>Time :-</td>
            <td  >{{events?.time}}</td> 
            <!-- Note:- 1) You can bind the ngClass by using string,array of string and object based on requirements 
                        2) You can not user ';' while property binding.
            -->
            <td [ngClass]="earlyStartClasses()" *ngSwitchCase="'8:00 am'" > (Early Start) </td>
            
            <!-- [ngStyle]="{color:events?.time ==='9:00 am'?'red':'black'}"  -->

            <td  [ngStyle]="lateStartStyle()" *ngSwitchCase="'9:00 am'" > (Late Start) </td>
            <td *ngSwitchDefault > (Normal Start) </td>
            </div>
        </tr>
    <tr>
        
        <td>{{events?.price | currency:'INR' }}</td>  <!-- <td>Price :-&#8377; </td> -->
    </tr>
    <div *ngIf="events.location" >
        <tr colspan="2">
        <td> location </td>
    </tr>
    <tr>
        <td> {{ events?.location?.city }} </td>
        <td> {{ events?.location?.country }} </td>
    </tr>
    </div>
    <div [hidden]="events?.onlineUrl" ></div> <!-- Note That by using this teqnique hidden property binding will hide the HTML element from the DOM.  -->
    <div *ngIf="events?.onlineUrl" >   <!-- note that *ngIf will completly removes the HTML element from the DOM.  -->

    <tr>
    <td> Online URL:-  </td><td> {{events?.onlineUrl}} </td>
</tr>
    </div>
</table>
<button class="btn btn-primary" (click)="onBtnClick(events)" >Click!</button>
    </div>
    `,
    styles: [`
    .thumbnail { min-height:210px; }    
    .green { color :#003300; }
    .bold { font-weight : bold;}
    `]
})
export class EventThumbnailsComponent {
    @Input() events: IEvent;
    @Output() onEventClick = new EventEmitter();
    /**
     * button Click method to emit the event
     * for it`s parents component.
     */
    onBtnClick = ($event) => this.onEventClick.emit(this.events);

    /**
     * this method is use to checking the accessibility of public method 
     * at outside this component by using the 
     * template`s local reference variable. 
     */
    onLocalVariableCheck = () => console.log("checking local variable...");
    /**
     * to check the 'ngClass' binding on template side. 
     */
    earlyStartClasses = () => {
        let isEarlyStart = this.events && this.events.time === '8:00 am';
        return { green: isEarlyStart, bold: isEarlyStart };
    };
    /**
     * to check th 'ngStyle' binding on template side.
     */
    lateStartStyle = () => this.events && this.events.time === "9:00 am" ? { color: 'red' } : { color: 'black' };

}
