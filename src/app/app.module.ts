import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { route } from './routes';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from "@angular/common/http";
/**
 * importing with the barrels
 */
import {
  CreateEventComponent,
  EventDetailsComponent,
  EventListComponent,
  EventListResolver,
  EventService,
  EventThumbnailsComponent,
  CreateSessionComponent,
  SessionListComponent,
  DurationPipe,
  UpvoteComponent,
  VoterService,
  LocationValidator,
  EventResolver
} from './events';
import {
  CollapsableWellComponent,
  JQ_TOKEN,
  TOASTR_TOKEN,
  Toastr,
  SimpleModalComponent,
  ModalTriggerDirective
}
  from './common';
import { AppComponent } from './app.component';
import { NavbarComponent } from './nav/navbar.component';
// import { ToastrService } from './common/toastr.service';
import { Error404Component } from './errors/404.component';
import { AuthService } from './user/auth.service';

let toastr: Toastr = window['toastr'];
let jQuery = window['$'];

@NgModule({
  declarations: [
    AppComponent,
    EventListComponent,
    EventThumbnailsComponent,
    NavbarComponent,
    EventDetailsComponent,
    CreateEventComponent,
    Error404Component,
    CreateSessionComponent,
    SessionListComponent,
    CollapsableWellComponent,
    DurationPipe,
    SimpleModalComponent,
    ModalTriggerDirective,
    UpvoteComponent,
    LocationValidator
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(route),
    HttpClientModule
  ],
  providers: [
    EventService,
    { provide: TOASTR_TOKEN, useValue: toastr },
    { provide: JQ_TOKEN, useValue: jQuery },
    // ToastrService,
    EventListResolver,
    EventResolver,
    AuthService,
    VoterService,
    { provide: 'canDeactivateCreateEvent', useValue: checkDirtyState },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
/**
 * the global function to check the dirty state of CreateEventComponent.  
 * @param component CreateEventComponent for referance to perform the action while leaving this component.
 * Note:- the 'export' object is not allow us to use the ES6 syntax.
 */
export function checkDirtyState(component: CreateEventComponent) {
  if (component.isDirty)
    return window.confirm("Do You Really wants to cancel ?");
  return true;
}